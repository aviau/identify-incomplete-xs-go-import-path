#!/usr/bin/python3

import requests
from debian.deb822 import Deb822
import sys
sys.path.append('dcs_python_client')
import swagger_client
from swagger_client.rest import ApiException

configuration = swagger_client.Configuration()
# API key specifically for https://salsa.debian.org/aviau/identify-incomplete-xs-go-import-path
# please see https://codesearch.debian.net/apikeys/ for details.
configuration.api_key[
    "x-dcs-apikey"
] = "MTYxNDkzMDg0NnxpajFnMGh5NTdBNnhteXBYN1Rmd1l1V2RldWZEeTZqcGVzekRPX3VlLUJUMzhkUmlNR243VzZIZXFzMHlzUzQwWFZMQko5QV9RN29TLWVLd3BpQ2EtOXY3THdYMWoyYmNnb3EyY05jai1abkx6MGdoMjFacHVPOXFrLURubUpnc1dIYm96aG1lUDJnZXpzRjFlUFNoVHZ0eXzQaGzKPLYH_pfqrX1YAHb05Udvo7KQsbE2cQrx_HbCMw=="

dcs = swagger_client.SearchApi(swagger_client.ApiClient(configuration))



#
# This program tries to identify go packages that have several Go import
# paths but don't identify it in XS-Go-Import-Path.
#
# It queries codesearch.debian.net using its OpenAPI interface:
# https://codesearch.debian.net/apikeys/
#

def inspect_source_package(source_package):
        import_paths = get_xs_go_import_paths(source_package)

        if len(import_paths) == 0:
            print(
                "{source_package}: does not even use XS-Go-Import-Path".format(
                    source_package=source_package,
                )
            )
        elif len(import_paths) == 1:
            print(
                "{source_package}: only has one import path".format(
                    source_package=source_package,
                )
            )
        else:
            print(
                "{source_package}: looks fine!".format(
                    source_package=source_package,
                )
            )


def get_control_file(source_package):
    r = requests.get(
        "https://sources.debian.org/api/src/{source_package}/unstable/debian/control".format(
            source_package=source_package,
        )
    )
    control_file_info = r.json()
    control_file_url = control_file_info["raw_url"]
    control_file = requests.get("https://sources.debian.org" + control_file_url).text
    return control_file


def get_xs_go_import_paths(source_package):
        control_file = get_control_file(source_package)
        deb822 = Deb822(control_file)

        xs_go_import_path = deb822.get("XS-Go-Import-Path", None)

        if xs_go_import_path is None:
            return []

        import_paths = xs_go_import_path.split(",")

        return import_paths


def get_matching_source_packages():
    dcs_query = "gocode path:debian/.*\.links package:golang.*"
    source_packages = set()
    try:
        # Searches through source code, see
        # https://codesearch.debian.net/apikeys/#/search/search
        api_response, status, headers = dcs.search_with_http_info(
            dcs_query, match_mode="regexp", _return_http_data_only=False
        )
        printed = 0
        for result in api_response:
            sp = result.package
            sp = sp[:sp.rfind('_')]
            source_packages.add(sp)
    except ApiException as e:
        print("Exception when calling SearchApi->search: %s\n" % e)
    return list(source_packages)


def inspect_source_packages(source_packages):
    for source_package in source_packages:
        try:
            inspect_source_package(source_package)
        except Exception as ex:
            print(
                "{source_package}: Skipping... ({message})".format(
                    source_package=source_package,
                    message=str(ex),
                )
            )
            continue


def main():
    source_packages = get_matching_source_packages()
    inspect_source_packages(source_packages)


if __name__ == "__main__":
    main()
