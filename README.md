# identify-incomplete-xs-go-import-path

This program tries to identify go packages that have several Go import
paths but don't identify them in XS-Go-Import-Path.

It queries codesearch.debian.net using its OpenAPI interface:
https://codesearch.debian.net/apikeys/


We should probably write a Lintian check for this:
- https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=984719
