# coding: utf-8

# flake8: noqa

"""
    Debian Code Search

    OpenAPI for https://codesearch.debian.net/  # noqa: E501

    OpenAPI spec version: 1.4.0
    Contact: stapelberg@debian.org
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""


from __future__ import absolute_import

# import apis into sdk package
from swagger_client.api.search_api import SearchApi

# import ApiClient
from swagger_client.api_client import ApiClient
from swagger_client.configuration import Configuration
# import models into sdk package
from swagger_client.models.package_search_result import PackageSearchResult
from swagger_client.models.search_result import SearchResult
